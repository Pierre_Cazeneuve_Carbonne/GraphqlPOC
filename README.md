# Projet GraphQL avec NestJS et Angular

## Backend: NestJS

### Installation et Démarrage

1. **Lancer la base de données**:
   ```bash
   docker-compose up
    ```
2. **Populer la base de données**:
    ```bash
    ts-node -O '{"module": "commonjs"}' seed.ts
     ```

3. **Lancer le serveur**:
   ```bash
   pnpm nx run back:serve
   ```
   
### Appolo Playground

Le backend intègre le Playground Apollo Server, une interface graphique pour tester et explorer votre API GraphQL.

Pour y accéder, rendez-vous sur http://localhost:3333/graphql

## Frontend: Angular

1. **Lancer le serveur**:
   ```bash
   pnpm nx run front:serve
   ```
Rendez-vous sur http://localhost:4200/articles.

