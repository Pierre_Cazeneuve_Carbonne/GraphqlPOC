import {Parent, ResolveField, Resolver} from "@nestjs/graphql";
import {ArticleService} from "../article.service";
import {UserService} from "../../user/user.service";
import {User} from "../../../graphql/generated/graphql";
import {Article as PrismaArticle} from "@prisma/client";

@Resolver('Article')
export class ArticleFieldsResolver {
  constructor(
    private articleService: ArticleService,
    private userService: UserService
  ) {}

    @ResolveField(() => User)
    async author(@Parent() article: PrismaArticle) {
        if (!article.authorId) return null
        try {
            const {user} =  await this.userService.getAuthorById(article.authorId)
            return user
        } catch (e) {
            console.log(e)
            return null
        }

  }


}
