import {Args, Query, Resolver} from '@nestjs/graphql';
import {ArticleService} from "../article.service";
import { ArticlesReadOutput, ArticleReadOutput} from "../dtos/article.read.dto";

@Resolver('Article')
export class ArticleQueriesResolver {
  constructor(
    private articleService: ArticleService
  ) {
  }

  @Query(() => ArticlesReadOutput)
  async articles() {
    const {articles} = await this.articleService.getArticles();
    return articles;
  }

  @Query(() => ArticleReadOutput)
  async article(@Args('id') id: string){
    const {article} = await this.articleService.getArticleById(id);
    return article;
  }

}
