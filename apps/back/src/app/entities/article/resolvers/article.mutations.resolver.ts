import {Args, Mutation, Resolver} from "@nestjs/graphql";
import {ArticleService} from "../article.service";
import {ArticleCreateInput, ArticleOutput} from "../dtos/article.create.dto";

@Resolver("Article")
export class ArticleMutationsResolver {
  constructor(
    private articleService: ArticleService
  ) {
  }

  @Mutation(() => ArticleOutput)
  async createArticle(
    @Args('articleCreateInput') articleCreateInput: ArticleCreateInput,

  ) {
    const article = await this.articleService.createArticle(articleCreateInput);
    return article;
  }
}
