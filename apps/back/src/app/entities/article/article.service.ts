import { Injectable } from '@nestjs/common';
import {ArticleCreateInput, ArticleOutput} from "./dtos/article.create.dto";
import {PrismaService} from "../../prisma/prisma.service";
import {ArticlesReadOutput} from "./dtos/article.read.dto";

@Injectable()
export class ArticleService {
  constructor(
    private prisma : PrismaService
  ) {
  }

  async getArticles(): Promise<ArticlesReadOutput> {
    const articles = await this.prisma.article.findMany();
    return {articles}
  }

  async getArticleById(id: string) {
    const article = await this.prisma.article.findUnique({
      where: {id},
    })
    return {article}
  }

  async createArticle(input: ArticleCreateInput): Promise<ArticleOutput> {
    const {authorEmail, ...data} = input
    const author = await this.prisma.user.findUnique({
      where: {
        email: authorEmail
      }
    })
    const article = await this.prisma.article.create({
        data: {
          ...data,
          authorId: author.id
        },
        include: {
          author: true
        }
      })
    return {article}

  }

}

