import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleQueriesResolver } from './resolvers/article.queries.resolver';
import {PrismaService} from "../../prisma/prisma.service";
import {ArticleMutationsResolver} from "./resolvers/article.mutations.resolver";
import {ArticleFieldsResolver} from "./resolvers/article.fields.resolver";
import {UserModule} from "../user/user.module";

@Module({
  imports: [UserModule],
  providers: [
    ArticleService,
    ArticleFieldsResolver,
    ArticleQueriesResolver,
    PrismaService,
    ArticleMutationsResolver
  ]
})
export class ArticleModule {}
