import {Field, InputType, ObjectType} from "@nestjs/graphql";
import {Article, User} from '../../../graphql/generated/graphql';
@ObjectType()
export class ArticlesOutput {
  @Field(() => [Article])
  articles: Article[];
}

@ObjectType()
export class ArticleOutput {
  @Field(() => Article)
  article: Article;
}

@InputType()
export class ArticleCreateInput {
  @Field(() => String, {nullable: false})
  title: string;
  @Field(() => String, {nullable: false})
  content: string;
  @Field(() => User['email'], {nullable: false})
  authorEmail: string;
}
