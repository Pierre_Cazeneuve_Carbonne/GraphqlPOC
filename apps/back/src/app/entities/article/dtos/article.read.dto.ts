import {Article} from "../../../graphql/generated/graphql"
import {Field, ObjectType} from "@nestjs/graphql";

@ObjectType()
export class ArticlesReadOutput {
  @Field(() => [Article])
  articles: Article[];
}

@ObjectType()
export class ArticleReadOutput {
    @Field(() => Article)
    article: Article;
}

@ObjectType()
export class ArticleCreateOutput {
  @Field(() => Article)
  article: Article;
}
