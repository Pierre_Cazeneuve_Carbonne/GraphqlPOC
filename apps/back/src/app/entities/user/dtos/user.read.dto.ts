import {User} from "../../../graphql/generated/graphql";
import {Field, ObjectType} from "@nestjs/graphql";

@ObjectType()
export class UserReadOutput {
  @Field(() => User)
  user: User
}
