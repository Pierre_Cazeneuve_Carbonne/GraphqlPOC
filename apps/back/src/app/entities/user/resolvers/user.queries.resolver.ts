import {Args, Parent, Query, Resolver} from "@nestjs/graphql";
import {UserService} from "../user.service";
import {UserReadOutput} from "../dtos/user.read.dto";

@Resolver('User')
export class UserQueriesResolver {
    constructor(
      private userService: UserService
    ) {
    }

    @Query(() => UserReadOutput)
    async user(
        @Args('id') id: string
    ) {
      const {user} =  await this.userService.getAuthorById(id)
      return user
    }


}
