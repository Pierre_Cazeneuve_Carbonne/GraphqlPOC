import { Module } from '@nestjs/common';
import {UserService} from "./user.service";
import {UserQueriesResolver} from "./resolvers/user.queries.resolver";
import {PrismaService} from "../../prisma/prisma.service";

@Module({
    exports: [UserService],
    providers: [UserService, UserQueriesResolver, PrismaService]
})
export class UserModule {}
