import { Injectable } from '@nestjs/common';
import {PrismaService} from "../../prisma/prisma.service";
import { UserReadOutput} from "./dtos/user.read.dto";

@Injectable()
export class UserService {

  constructor(
    private prisma: PrismaService
  ) {
  }


  async getAuthorById(id: string): Promise<UserReadOutput | null> {
    const user = await this.prisma.user.findUnique({
      where: { id },
    })

    if (!user) {
      return null;
    }

    return { user };
  }
}
