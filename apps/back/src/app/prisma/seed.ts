import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  for (let i = 1; i <= 10; i++) {
    const email = `user${i}@example.com`;
    const name = `User ${i}`;

    const user = await prisma.user.create({
      data: {
        email: email,
        name: name,
        articles: {
          create: Array.from({ length: 10 }).map((_, j) => ({
            title: `Article ${j + 1} de ${name}`,
            content: `Contenu de l'article ${j + 1} écrit par ${name}`,
          })),
        },
      },
    });

    console.log(`Created user with email: ${user.email}`);
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
