import { mergeTypeDefs } from '@graphql-tools/merge';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import glob from 'glob';
import { print } from 'graphql';

const schemas = glob
  .sync('../**/*.graphql')
  .map((path) => readFileSync(join(__dirname, path), 'utf8'));

const mergedSchemaAST = mergeTypeDefs(schemas);
const mergedSchemaString = print(mergedSchemaAST);
writeFileSync(join(__dirname, './generated/schema.graphql'), mergedSchemaString);
