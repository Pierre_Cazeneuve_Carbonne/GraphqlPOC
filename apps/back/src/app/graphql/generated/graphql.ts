
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class ArticleCreateInput {
    title: string;
    content: string;
    authorEmail: string;
}

export class Article {
    __typename?: 'Article';
    id: string;
    title: string;
    content: string;
    author?: User;
}

export abstract class IQuery {
    __typename?: 'IQuery';

    abstract articles(): Article[] | Promise<Article[]>;

    abstract article(id: string): Article | Promise<Article>;

    abstract user(id: string): Nullable<User> | Promise<Nullable<User>>;
}

export abstract class IMutation {
    __typename?: 'IMutation';

    abstract createArticle(input?: Nullable<ArticleCreateInput>): Article | Promise<Article>;
}

export class User {
    __typename?: 'User';
    id: string;
    name: string;
    email: string;
    articles?: Nullable<Article[]>;
}

type Nullable<T> = T | null;
