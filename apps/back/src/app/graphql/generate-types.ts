import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['../**/*.graphql'],
  path: join(process.cwd(), '/generated/graphql.ts'),
  outputAs: 'class',
  watch: true,
  emitTypenameField: true,
});
