
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export interface ArticleCreateInput {
    title: string;
    content: string;
    authorEmail: string;
}

export interface Article {
    id: string;
    title: string;
    content: string;
    author: User;
}

export interface IQuery {
    articles(): Article[] | Promise<Article[]>;
    article(id: string): Article | Promise<Article>;
    user(id: string): Nullable<User> | Promise<Nullable<User>>;
}

export interface IMutation {
    createArticle(input?: Nullable<ArticleCreateInput>): Article | Promise<Article>;
}

export interface User {
    id: string;
    name: string;
    email: string;
    articles?: Nullable<Article[]>;
}

type Nullable<T> = T | null;
