import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: 'articles',
        loadComponent: () => import('./articles/articles.component').then(c => c.ArticlesComponent)
    }
];
