import {Component, OnInit} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterLink} from "@angular/router";
import { Apollo } from 'apollo-angular';
import {GET_ARTICLES} from "./articles.operations";

@Component({
    standalone: true,
    selector: 'protocols-articles',
    imports: [CommonModule, RouterLink],
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css'],
})
export class ArticlesComponent implements OnInit {
    loading  = true;
    articles: any[] =[]

    constructor(private apollo : Apollo){}

    ngOnInit(): void {
        this.loadCountries();
    }

    loadCountries(){
        this.apollo.watchQuery({
            query: GET_ARTICLES
        }).valueChanges.subscribe(({data, error} : any) => {
            this.loading = false;
            this.articles = data.articles;
        })
    }
}
