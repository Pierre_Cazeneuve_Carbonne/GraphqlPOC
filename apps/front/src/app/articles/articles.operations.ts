import {gql} from "apollo-angular";

export const GET_ARTICLES = gql`
    query Articles {
        articles {
            id
            title
            content
        }
    }
`
