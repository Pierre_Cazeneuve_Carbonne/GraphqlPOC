import { Component } from '@angular/core';
import {ArticlesComponent} from "./articles/articles.component";
import {RouterLink, RouterOutlet} from "@angular/router";

@Component({
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink,
    ArticlesComponent
  ],
  selector: 'protocols-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'front';
}
